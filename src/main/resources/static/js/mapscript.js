/**
 * Created by baran on 22.05.16.
 */

/*<![CDATA[*/
var zoom = 11;

var mymap = L.map('mapid').setView([50.068, 19.912], zoom);
L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpandmbXliNDBjZWd2M2x6bDk3c2ZtOTkifQ._QA7i5Mpkd_m30IGElHziw', {
    maxZoom: zoom,
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
    '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
    'Imagery © <a href="http://mapbox.com">Mapbox</a>',
    id: 'mapbox.streets'
}).addTo(mymap);

var marker = L.marker();

function onMapClick(e) {
    marker.setLatLng(e.latlng).addTo(mymap);
    marker.bindPopup("Click to remove", {closeButton : false}).openPopup();
    document.getElementById("lat").value = e.latlng.lat;
    document.getElementById("lng").value = e.latlng.lng;
}

function onMarkerClick() {
    mymap.removeLayer(marker);
    document.getElementById("lat").value = null;
    document.getElementById("lng").value = null;
}

mymap.on('click', onMapClick);
marker.on('click', onMarkerClick);

function center(lat, lng){
    if(lat != null || lng != null) {
        var latLng = new L.LatLng(lat, lng);
        mymap.setView(latLng, zoom);
        marker.setLatLng(latLng).addTo(mymap);
    }
}


/*]]>*/