package ic.utility;

import java.util.Arrays;
import java.util.HashSet;

public class GeneralUtility {
    @SafeVarargs
    public static <T> HashSet<T> hashSetOfElements(T... elements) {
        return new HashSet<T>(Arrays.asList(elements));
    }
}
