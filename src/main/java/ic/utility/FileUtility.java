package ic.utility;

import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Created by piotrek on 23.05.2016.
 */
public class FileUtility {

    public static byte[] multiPartFile2ByteArray(MultipartFile file, int width, int height) throws IOException {
        if(file != null){
            ByteArrayInputStream bis = new ByteArrayInputStream(file.getBytes());
            BufferedImage bufferedImage = ImageIO.read(bis);
            if(bufferedImage != null){
                Image scaledImage = bufferedImage.getScaledInstance(width, height, Image.SCALE_SMOOTH);
                BufferedImage scaledBufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
                Graphics2D g2 = scaledBufferedImage.createGraphics();
                g2.drawImage(scaledImage, 0, 0, null);
                g2.dispose();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ImageIO.write(scaledBufferedImage, "jpg", baos );
                baos.flush();
                byte[] imageInByte = baos.toByteArray();
                baos.close();
                return imageInByte;
            }
        }
        return null;
    }

}
