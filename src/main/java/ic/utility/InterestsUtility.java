package ic.utility;

import ic.domain.interests.Interest;
import ic.domain.interests.InterestLikeLevel;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

public class InterestsUtility {
    public static void setChildParentRelationship(Interest child, Interest parent) {
        child.addParentName(parent.getName());
        parent.addChildName(child.getName());
    }

    public static void setParentChildRelationship(Interest parent, Interest child) {
        setChildParentRelationship(child, parent);
    }

    public static void unsetChildParentRelationship(Interest child, Interest parent) {
        child.removeParentName(parent.getName());
        parent.removeChildName(child.getName());
    }

    public static void unsetParentChildRelationship(Interest parent, Interest child) {
        unsetChildParentRelationship(child, parent);
    }

    public static Set<String> getInterestWithItsParentsAndChildren(Interest interest) {
        Set<String> displayedNonRootInterestsNames = new HashSet<>();
        displayedNonRootInterestsNames.add(interest.getName());
        displayedNonRootInterestsNames.addAll(interest.getChildrenNames());
        displayedNonRootInterestsNames.addAll(interest.getParentsNames());

        return displayedNonRootInterestsNames;
    }

    public static Map<InterestLikeLevel, Map<String, Integer>> newInterestsLikeLevelCounts() {
        Map<InterestLikeLevel, Map<String, Integer>> interestsLikeLevelCounts = new HashMap<>();
        for (InterestLikeLevel likeLevel : InterestLikeLevel.values()) {
            interestsLikeLevelCounts.put(likeLevel, new HashMap<>());
        }

        return interestsLikeLevelCounts;
    }

    public static void overviewOfUserInterestsComparingToActiveUser(Map<String, InterestLikeLevel> userLikedInterestsNames,
                                                                    Map<String, InterestLikeLevel> activeUserLikedInterestsNames,
                                                                    Map<InterestLikeLevel, Set<String>> commonEquallyLikedInterestsNames,
                                                                    Map<InterestLikeLevel, Set<String>> commonUserLikedInterestsNames,
                                                                    Map<InterestLikeLevel, Set<String>> individualUserInterestsNames) {
        userLikedInterestsNames.forEach((name, level) -> {
            if (activeUserLikedInterestsNames.containsKey(name)) {
                if (userLikedInterestsNames.get(name) == activeUserLikedInterestsNames.get(name)) {
                    addInterestNameToInterestLikeLevelSet(commonEquallyLikedInterestsNames, name, level);
                } else {
                    addInterestNameToInterestLikeLevelSet(commonUserLikedInterestsNames, name, userLikedInterestsNames.get(name));
                }
            } else {
                addInterestNameToInterestLikeLevelSet(individualUserInterestsNames, name, level);
            }
        });
    }

    private static void addInterestNameToInterestLikeLevelSet(Map<InterestLikeLevel, Set<String>> interestsNames, String name, InterestLikeLevel level) {
        interestsNames.putIfAbsent(level, new HashSet<>());
        interestsNames.get(level).add(name);
    }
}
