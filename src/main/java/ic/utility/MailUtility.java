package ic.utility;

import ic.domain.users.User;
import org.springframework.mail.SimpleMailMessage;

public class MailUtility {
    public static SimpleMailMessage getPasswordRecoveryMessage( User user, String newPassword ) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setSubject( "inCommon password recovery" );
        mailMessage.setTo( user.getMail() );
        mailMessage.setText( "Hi " + user.getUsername() + ", your new password is: " + newPassword );

        return mailMessage;
    }
}
