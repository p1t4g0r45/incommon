package ic.utility;

import ic.domain.users.User;

public class UsersUtility {
    public static double getUserInterestsRatio( User user, long totalInterestCount ) {
        double userInterests = user.getLikedInterestsNames().size();
        return userInterests / totalInterestCount;
    }
}
