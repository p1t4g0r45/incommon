package ic.repositories;

import ic.domain.reports.Report;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

public interface ReportsRepository extends MongoRepository<Report, String>, QueryDslPredicateExecutor<Report> {

}
