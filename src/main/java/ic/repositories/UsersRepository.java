package ic.repositories;

import ic.domain.users.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

/**
 * Created by ludwikbukowski on 05/04/16.
 */
public interface UsersRepository extends MongoRepository<User, String>, QueryDslPredicateExecutor<User> {

}
