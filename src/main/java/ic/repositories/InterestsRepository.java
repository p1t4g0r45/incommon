package ic.repositories;

import ic.domain.interests.Interest;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import java.util.List;

public interface InterestsRepository extends MongoRepository<Interest, String>, QueryDslPredicateExecutor<Interest> {
    //Interest|List<Interest> findBySynonymsContaining(String synonym);
}
