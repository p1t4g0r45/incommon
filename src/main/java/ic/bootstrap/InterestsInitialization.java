package ic.bootstrap;

import static ic.domain.interests.Interest.ROOT_INTEREST_NAME;

import ic.domain.interests.Interest;
import ic.repositories.InterestsRepository;
import ic.utility.InterestsUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Component
public class InterestsInitialization implements ApplicationListener<ContextRefreshedEvent> {
    private static final Set<String> INITIAL_INTERESTS_NAMES;

    static {
        INITIAL_INTERESTS_NAMES = new HashSet<>();
        INITIAL_INTERESTS_NAMES.add("Music");
        INITIAL_INTERESTS_NAMES.add("Sport");
        INITIAL_INTERESTS_NAMES.add("Film");
    }

    private InterestsRepository interestsRepository;

    @Autowired
    public void setInterestsRepository(InterestsRepository interestsRepository) {
        this.interestsRepository = interestsRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        if (!interestsRepository.exists(ROOT_INTEREST_NAME)) {
            Interest root = new Interest(ROOT_INTEREST_NAME, Collections.emptySet(), new HashSet<>());

            for (String name : INITIAL_INTERESTS_NAMES) {
                Interest child = new Interest(name, new HashSet<>(), new HashSet<>());

                InterestsUtility.setParentChildRelationship( root, child);

                interestsRepository.save(child);
            }

            interestsRepository.save(root);

            Interest sport = interestsRepository.findOne("Sport");

            Interest football = new Interest("Football", new HashSet<>(), new HashSet<>());
            InterestsUtility.setChildParentRelationship( football, sport);

            Interest basketball = new Interest("Basketball", new HashSet<>(), new HashSet<>());
            InterestsUtility.setChildParentRelationship( basketball, sport);

            interestsRepository.save(sport);
            interestsRepository.save(football);
            interestsRepository.save(basketball);
        }
    }
}
