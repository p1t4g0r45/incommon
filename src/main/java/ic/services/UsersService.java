package ic.services;

import java.util.List;
import java.util.Map;

import ic.domain.interests.InterestLikeLevel;
import ic.domain.users.User;

/**
 * Created by ludwikbukowski on 05/04/16.
 */
public interface UsersService {
    User updateUser( User user );

    User saveUser( User user );

    Map<InterestLikeLevel, Integer> getInterestPopularityAmongUsers( String interestName );

    List<User> getAllUsers();

    User getUserByUsername(String username);

    void addUser(User user);

    void renameInterest(String oldInterestName, String newInterestName);

    void removeInterest(String interestName);
}
