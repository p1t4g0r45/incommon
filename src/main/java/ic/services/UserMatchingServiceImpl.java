package ic.services;

import javax.annotation.Nullable;
import java.util.*;
import java.util.stream.Collectors;

import com.mysema.query.BooleanBuilder;
import ic.domain.interests.Interest;
import ic.domain.interests.InterestLikeLevel;
import ic.domain.users.AgeBounds;
import ic.domain.users.Gender;
import ic.domain.users.QUser;
import ic.domain.users.User;
import ic.repositories.InterestsRepository;
import ic.repositories.UsersRepository;
import ic.utility.UsersUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserMatchingServiceImpl implements UserMatchingService {
    private static final Logger LOGGER = LoggerFactory.getLogger( UserMatchingServiceImpl.class );
    private static final double SAME_INTEREST_FACTOR = 1.0;
    private static final double NEIHGBOUR_INTEREST_DIVISION_FACTOR = 2;
    private static final int MAX_SEARCH_RESULTS = 10;
    private static final int SEARCH_RESUTLS = 10;

    private UsersRepository usersRepository;
    private InterestsRepository interestsRepository;

    @Autowired
    public void setUsersRepository( UsersRepository usersRepository ) {
        this.usersRepository = usersRepository;
    }

    @Autowired
    public void setInterestsRepository( InterestsRepository interestsRepository ) {
        this.interestsRepository = interestsRepository;
    }

    @Override
    public List<User> getTopMatchedUsers( String username ) {
        LOGGER.info( "Started comparing users for user " + username );

        User user = usersRepository.findOne( username );
        LOGGER.info( username + "'s interests: " + user.getLikedInterestsNames().toString() );

        long totalInterestCount = interestsRepository.count();

        double userLikeWeight = 1 - UsersUtility.getUserInterestsRatio( user, totalInterestCount );
        LOGGER.info( username + "'s like weight: " + userLikeWeight );

        Optional<Gender> genderOptional = Optional.ofNullable( user.getPreferredGender() );
        Optional<AgeBounds> ageBoundsOptional = Optional.ofNullable( user.getPreferredAgeBounds() );

        QUser anyUser = QUser.user;
        BooleanBuilder where = new BooleanBuilder();
        where.and( anyUser.username.notLike( username ) );
        if ( genderOptional.isPresent() && user.getGender() != Gender.UNDEFINED ) {
            where.and( anyUser.gender.eq( genderOptional.get() ) );
        }

        TreeMap<Double, List<User>> map = new TreeMap<>( ( Double one, Double two ) -> two.compareTo( one ) );

        for ( User comparedUser : usersRepository.findAll( where ) ) {

            if(ageBoundsOptional.isPresent() && user.getAge() != null && (comparedUser.getAge() == null ||
                    comparedUser.getAge() > ageBoundsOptional.get().getUpperAgeBounds() ||
                    comparedUser.getAge() < ageBoundsOptional.get().getLowerAgeBounds())) {
                        continue;
            }

            if(user.getPreferredDistance() != null &&
                    user.getLatLng() != null &&
                    user.getLatLng().isSet() && (
                    comparedUser.getLatLng() == null ||
                    !comparedUser.getLatLng().isSet() ||
                    distance(user.getLatLng().getLat(),
                        comparedUser.getLatLng().getLat(),
                        user.getLatLng().getLng(),
                        comparedUser.getLatLng().getLng(),
                        0.0, 0.0
                    ) / 1000 > user.getPreferredDistance())){
                System.out.print(comparedUser.getUsername() + " thrown");
                        continue;
            }

            String comparedUserUsername = comparedUser.getUsername();
            LOGGER.info( "Comparing user " + comparedUserUsername + " for user " + username );
            LOGGER.info( comparedUserUsername + "'s interests: " + comparedUser.getLikedInterestsNames().toString() );
            double comparedUserLikeWeight = 1 - UsersUtility.getUserInterestsRatio( user, totalInterestCount );

            double score = compareAndGetScore( interestsNamesMapToInterestsMap( user.getLikedInterestsNames() ),
                                               interestsNamesMapToInterestsMap( comparedUser.getLikedInterestsNames() ),
                                               userLikeWeight,
                                               comparedUserLikeWeight );
            if ( score > 0 ) {
                if ( !map.containsKey( score ) ) {
                    map.put( score, new ArrayList<>() );
                }
                map.get( score ).add( comparedUser );
            }
        }

        List<User> matchedUsers = map.values().stream().flatMap( Collection::stream ).collect( Collectors.toList() );
        matchedUsers = matchedUsers.subList( 0, Math.min( matchedUsers.size(), SEARCH_RESUTLS ) );

        user.setPotentialFriendsNames(matchedUsers.stream().map(User::getUsername).collect(Collectors.toSet()));
        usersRepository.save(user);

        return matchedUsers;
    }

    private Map<Interest, InterestLikeLevel> interestsNamesMapToInterestsMap( Map<String, InterestLikeLevel>
                                                                                      likedInterestsNames ) {
        Map<Interest, InterestLikeLevel> map = new HashMap<>();

        likedInterestsNames.forEach( ( i, l ) -> map.put( interestsRepository.findOne( i ), l ) );

        return map;
    }

    private double compareAndGetScore( Map<Interest, InterestLikeLevel> userInterests,
                                       Map<Interest, InterestLikeLevel> comparedUserInterests,
                                       double userLikeWeight,
                                       double comparedUserLikeWeight ) {
        double score = 0.0;
        double interestEqualityFactor = SAME_INTEREST_FACTOR;

        Set<Interest> userInterestSet = new HashSet<>( userInterests.keySet() );
        Set<Interest> comparedUserInterestSet = new HashSet<>( comparedUserInterests.keySet() );
        Set<Interest> processedInterests = new HashSet<>();

        LOGGER.info( "Factor " + interestEqualityFactor );

        Set<Interest> commonInterestsSet = new HashSet<>( userInterestSet );
        commonInterestsSet.retainAll( comparedUserInterestSet );

        score += commonInterestsFormula( commonInterestsSet,
                                         userInterests,
                                         comparedUserInterests,
                                         userLikeWeight,
                                         comparedUserLikeWeight );

        /* Mark all of the user's interests as processed */
        processedInterests.addAll( userInterestSet );

        Set<Interest> currentSetChildren = new HashSet<>();
        Set<Interest> currentSetParents = new HashSet<>();

        for ( Interest interest : userInterestSet ) {
            for ( String childName : interest.getChildrenNames() ) {
                Interest child = interestsRepository.findOne( childName );
                if ( !processedInterests.contains( child ) ) {
                    processedInterests.add( child );
                    currentSetChildren.add( child );
                }

            }
            for ( String parentName : interest.getParentsNames() ) {
                Interest parent = interestsRepository.findOne( parentName );
                if ( !processedInterests.contains( parent ) ) {
                    processedInterests.add( parent );
                    currentSetParents.add( parent );
                }
            }
        }

        while ( !currentSetChildren.isEmpty() && !currentSetParents.isEmpty() ) {
            interestEqualityFactor /= NEIHGBOUR_INTEREST_DIVISION_FACTOR;

            LOGGER.info( "Factor " + interestEqualityFactor );

            score += similarInterestsFormula( currentSetChildren,
                                              interestEqualityFactor,
                                              comparedUserInterests,
                                              userLikeWeight,
                                              comparedUserLikeWeight );
            score += similarInterestsFormula( currentSetParents,
                                              interestEqualityFactor,
                                              comparedUserInterests,
                                              userLikeWeight,
                                              comparedUserLikeWeight );

            for ( Iterator<Interest> i = currentSetChildren.iterator(); i.hasNext(); ) {
                Interest child = i.next();
                i.remove();
                for ( String childName : child.getChildrenNames() ) {
                    Interest newChild = interestsRepository.findOne( childName );
                    if ( !processedInterests.contains( newChild ) ) {
                        processedInterests.add( child );
                        currentSetChildren.add( newChild );
                    }
                }
            }
            for ( Iterator<Interest> i = currentSetParents.iterator(); i.hasNext(); ) {
                Interest parent = i.next();
                i.remove();
                for ( String parentName : parent.getParentsNames() ) {
                    Interest newParent = interestsRepository.findOne( parentName );
                    if ( !processedInterests.contains( newParent ) ) {
                        processedInterests.add( parent );
                        currentSetParents.add( newParent );
                    }
                }
            }

        }

        LOGGER.info( "Score =" + score );
        return score;
    }

    private static double commonInterestsFormula( Set<Interest> commonInterests,
                                                  Map<Interest, InterestLikeLevel> userInterests,
                                                  Map<Interest, InterestLikeLevel> comparedUserInterests,
                                                  double userLikeWeight,
                                                  double comparedUserLikeWeight ) {
        double score = 0;

        for ( Interest interest : commonInterests ) {
            score += Math.pow( userLikeWeight * userInterests.get( interest ).getFactor(), 2 ) *
                     comparedUserLikeWeight * comparedUserInterests.get( interest ).getFactor();
        }

        LOGGER.info( "Score +" + score );
        return score;
    }

    private static double similarInterestsFormula( Set<Interest> interests,
                                                   double factor,
                                                   Map<Interest, InterestLikeLevel> comparedUserInterests,
                                                   double userLikeWeight,
                                                   double comparedUserLikeWeight ) {
        if ( Collections.disjoint( interests, comparedUserInterests.keySet() ) ) {
            return 0;
        }

        double score = 0;

        for ( Interest interest : interests ) {
            Optional<InterestLikeLevel> comparedUserLikeLevel = Optional.ofNullable( comparedUserInterests.get(
                    interest ) );

            score += factor * userLikeWeight * comparedUserLikeWeight *
                     comparedUserLikeLevel.map( InterestLikeLevel::getFactor ).orElse( 0.0 );
        }

        LOGGER.info( "Score +" + score );
        return score;
    }

    private static double distance(double lat1, double lat2, double lon1, double lon2, double el1, double el2) {

        final int R = 6371; // Radius of the earth

        Double latDistance = Math.toRadians(lat2 - lat1);
        Double lonDistance = Math.toRadians(lon2 - lon1);
        Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to meters

        double height = el1 - el2;

        distance = Math.pow(distance, 2) + Math.pow(height, 2);

        return Math.sqrt(distance);
    }
}
