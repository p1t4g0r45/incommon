package ic.services;

import ic.domain.reports.Report;

import java.util.List;

public interface ReportsService {
    List<Report> getAll();

    Report getReport(String id);

    Report saveReport(Report report);

    void deleteReport(String id);
}
