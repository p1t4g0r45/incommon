package ic.services;

import java.util.List;
import java.util.Set;

import ic.domain.interests.Interest;

public interface InterestsService {
    Interest getInterestByName( String name );

    Interest getRootInterest();

    List<String> getInterestsNamesWithNameContaining( String str, Integer count );

    long getTotalInterestCount();

    void addInterest(String name, Set<String> parentsNames, Set<String> childrenNames );

    void renameInterest(String oldName, String newName);

    void removeInterest(String name);

    void modifyInterest(Interest interest, Set<String> parentsNames, Set<String> childrenNames);
}
