package ic.services;

import com.google.common.collect.Lists;
import com.mysema.query.BooleanBuilder;
import ic.domain.interests.Interest;
import ic.domain.interests.QInterest;
import ic.repositories.InterestsRepository;
import ic.utility.InterestsUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class InterestsServiceImpl implements InterestsService {

    private InterestsRepository interestsRepository;

    @Autowired
    public void setInterestsRepository(InterestsRepository interestsRepository) {
        this.interestsRepository = interestsRepository;
    }

    @Override
    public Interest getInterestByName(String name) {
        return interestsRepository.findOne(name);
    }

    @Override
    public Interest getRootInterest() {
        return getInterestByName(Interest.ROOT_INTEREST_NAME);
    }

    @Override
    public List<String> getInterestsNamesWithNameContaining(String str, Integer count) {
        QInterest anyInterest = QInterest.interest;
        BooleanBuilder where = new BooleanBuilder();
        where.and(anyInterest.name.containsIgnoreCase(str));

        List<Interest> foundInterests = Lists.newArrayList(interestsRepository.findAll(where));
        foundInterests.removeIf(interest -> interest.equals(getRootInterest()));

        double queryLength = str.length();
        foundInterests.sort((Interest one, Interest two) -> new Double(queryLength / two.getName().length()).compareTo(queryLength / one.getName().length()));

        if (count != null) {
            foundInterests = foundInterests.subList(0, Math.min(foundInterests.size(), count));
        }

        return foundInterests.stream()
                .map(Interest::getName)
                .collect(Collectors.toList());
    }

    @Override
    public long getTotalInterestCount() {
        return interestsRepository.count() - 1; // minus the root interest
    }

    @Override
    public void addInterest(String name, Set<String> parentsNames, Set<String> childrenNames) {
        Interest newInterest = new Interest(name, parentsNames, childrenNames);

        Set<Interest> parents = interestsNamesSetToInterestsSet(parentsNames);
        Set<Interest> children = interestsNamesSetToInterestsSet(childrenNames);

        // Now we check if the new interest is between some other two interests and if it is we change relationships
        for (Interest parent : parents) {
            if (!Collections.disjoint(parent.getChildrenNames(), childrenNames)) {
                Set<String> commonChildrenNames = new HashSet<>(parent.getChildrenNames());
                commonChildrenNames.retainAll(childrenNames);
                Set<Interest> commonChildren = interestsNamesSetToInterestsSet(commonChildrenNames);
                commonChildren.forEach(child -> InterestsUtility.unsetParentChildRelationship(parent, child));
            }
        }
        for (Interest child : children) {
            if (!Collections.disjoint(child.getParentsNames(), parentsNames)) {
                Set<String> commonParentsNames = new HashSet<>(child.getParentsNames());
                commonParentsNames.retainAll(parentsNames);
                Set<Interest> commonParents = interestsNamesSetToInterestsSet(commonParentsNames);
                commonParents.forEach(parent -> InterestsUtility.unsetChildParentRelationship(child, parent));
            }
        }

        // Adding relationships between the new interest and its parents and children and saving
        parents.forEach(p -> {
            InterestsUtility.setParentChildRelationship(p, newInterest);
            interestsRepository.save(p);
        });
        children.forEach(c -> {
            InterestsUtility.setChildParentRelationship(c, newInterest);
            interestsRepository.save(c);
        });

        interestsRepository.save(newInterest);
    }

    @Override
    public void renameInterest(String oldName, String newName) {
        Interest interest = getInterestByName(oldName);

        interestsRepository.delete(interest);

        Set<Interest> parents = interestsNamesSetToInterestsSet(interest.getParentsNames());
        Set<Interest> children = interestsNamesSetToInterestsSet(interest.getChildrenNames());

        parents.forEach(p -> {
            InterestsUtility.unsetParentChildRelationship(p, interest);
        });
        children.forEach(c -> {
            InterestsUtility.unsetChildParentRelationship(c, interest);
        });

        interest.setName(newName);

        parents.forEach(p -> {
            InterestsUtility.setParentChildRelationship(p, interest);
            interestsRepository.save(p);
        });
        children.forEach(c -> {
            InterestsUtility.setChildParentRelationship(c, interest);
            interestsRepository.save(c);
        });

        interestsRepository.save(interest);
    }

    @Override
    public void removeInterest(String name) {
        Interest interest = getInterestByName(name);

        interestsRepository.delete(interest);

        Set<Interest> parents = interestsNamesSetToInterestsSet(interest.getParentsNames());
        Set<Interest> children = interestsNamesSetToInterestsSet(interest.getChildrenNames());

        parents.forEach(p -> {
            InterestsUtility.unsetParentChildRelationship(p, interest);
            children.forEach(c -> {
                InterestsUtility.setParentChildRelationship(p, c);
            });
            interestsRepository.save(p);
        });
        children.forEach(c -> {
            InterestsUtility.unsetChildParentRelationship(c, interest);
            parents.forEach(p -> {
                InterestsUtility.setChildParentRelationship(c, p);
            });
            interestsRepository.save(c);
        });
    }

    @Override
    public void modifyInterest(Interest interest, Set<String> parentsNames, Set<String> childrenNames) {
        Set<Interest> oldParents = interestsNamesSetToInterestsSet(interest.getParentsNames());
        Set<Interest> oldChildren = interestsNamesSetToInterestsSet(interest.getChildrenNames());

        oldParents.forEach(p -> {
            InterestsUtility.unsetParentChildRelationship(p, interest);
            interestsRepository.save(p);
        });
        oldChildren.forEach(c -> {
            InterestsUtility.unsetChildParentRelationship(c, interest);
            interestsRepository.save(c);
        });

        Set<Interest> newParents = interestsNamesSetToInterestsSet(parentsNames);
        Set<Interest> newChildren = interestsNamesSetToInterestsSet(childrenNames);

        newParents.forEach(p -> {
            InterestsUtility.setParentChildRelationship(p, interest);
            interestsRepository.save(p);
        });
        newChildren.forEach(c -> {
            InterestsUtility.setChildParentRelationship(c, interest);
            interestsRepository.save(c);
        });

        interestsRepository.save(interest);
    }

    private Set<Interest> interestsNamesSetToInterestsSet(Set<String> interestsNames) {
        Set<Interest> interests = new HashSet<>();
        interestsNames.forEach(p -> interests.add(getInterestByName(p)));

        return interests;
    }
}
