package ic.services;

import com.mysema.query.BooleanBuilder;
import ic.domain.interests.InterestLikeLevel;
import ic.domain.users.QUser;
import ic.domain.users.User;
import ic.repositories.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UsersServiceImpl implements UsersService {

    private UsersRepository usersRepository;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder){
        this.passwordEncoder = passwordEncoder;
    }

    @Autowired
    public void setUsersRepository(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    @Override
    public List<User> getAllUsers() {
        return usersRepository.findAll();
    }

    @Override
    public User getUserByUsername(String username) {
        return usersRepository.findOne(username);
    }

    @Override
    public void addUser(User user){
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        usersRepository.save(user);
    }

    @Override
    public User updateUser(User user) {
        String activeUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        User currentUser = usersRepository.findOne(activeUsername);
        currentUser.setAvatar(user.getAvatar());
        currentUser.setName(user.getName());
        currentUser.setSurname(user.getSurname());
        currentUser.setGender(user.getGender());
        currentUser.setBirthday(user.getBirthday());
        currentUser.setMail(user.getMail());
        currentUser.setLatLng(user.getLatLng());
        usersRepository.save(currentUser);
        return user;
    }

    @Override
    public User saveUser(User user) {
        usersRepository.save(user);
        return user;
    }

    @Override
    public Map<InterestLikeLevel, Integer> getInterestPopularityAmongUsers( String interestName ) {
        QUser anyUser = QUser.user;
        BooleanBuilder where = new BooleanBuilder();
        where.and( anyUser.likedInterestsNames.containsKey( interestName ));

        Map<InterestLikeLevel, Integer> popularity = new HashMap<>();
        for( InterestLikeLevel ill : InterestLikeLevel.values()) {
            popularity.put( ill, 0 );
        }

        for ( User user : usersRepository.findAll(where) ) {
            InterestLikeLevel level = user.getInterestNameLikeLevel( interestName );
            popularity.put(level, popularity.get(level) + 1);
        }

        return popularity;
    }

    @Override
    public void renameInterest(String oldInterestName, String newInterestName) {
        QUser anyUser = QUser.user;
        BooleanBuilder where = new BooleanBuilder();
        where.and( anyUser.likedInterestsNames.containsKey( oldInterestName ));

        for ( User user : usersRepository.findAll(where) ) {
            Map<String, InterestLikeLevel> userLikedInterests = user.getLikedInterestsNames();
            InterestLikeLevel level = userLikedInterests.get(oldInterestName);
            userLikedInterests.remove(oldInterestName);
            userLikedInterests.put(newInterestName, level);
            usersRepository.save(user);
        }
    }

    @Override
    public void removeInterest(String interestName) {
        QUser anyUser = QUser.user;
        BooleanBuilder where = new BooleanBuilder();
        where.and( anyUser.likedInterestsNames.containsKey( interestName ));

        for ( User user : usersRepository.findAll(where) ) {
            user.getLikedInterestsNames().remove(interestName);
            usersRepository.save(user);
        }
    }
}
