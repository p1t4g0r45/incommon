package ic.services;

import ic.domain.users.AgeBounds;
import ic.domain.users.Gender;
import ic.domain.users.User;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Optional;

public interface UserMatchingService {
    List<User> getTopMatchedUsers(String username);
}
