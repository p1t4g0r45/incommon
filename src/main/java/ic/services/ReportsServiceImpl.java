package ic.services;

import ic.domain.reports.Report;
import ic.repositories.ReportsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReportsServiceImpl implements ReportsService {
    private ReportsRepository reportsRepository;

    @Autowired
    public void setReportsRepository(ReportsRepository reportsRepository) {
        this.reportsRepository = reportsRepository;
    }

    @Override
    public List<Report> getAll() {
        return reportsRepository.findAll();
    }

    @Override
    public Report getReport(String id) {
        return reportsRepository.findOne(id);
    }

    @Override
    public Report saveReport(Report report) {
        return reportsRepository.save(report);
    }

    @Override
    public void deleteReport(String id) {
        reportsRepository.delete(id);
    }

}
