package ic.controllers;

import ic.domain.reports.Report;
import ic.domain.users.User;
import ic.services.ReportsService;
import ic.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@Controller
public class ReportsController {
    private ReportsService reportsService;
    private UsersService usersService;

    @Autowired
    public void setReportsService(ReportsService reportsService) {
        this.reportsService = reportsService;
    }

    @Autowired
    public void setUsersService(UsersService usersService) {
        this.usersService = usersService;
    }

    @RequestMapping(value = "/reports", method = RequestMethod.GET)
    public String getReports(Model model) {
        model.addAttribute("reports", reportsService.getAll());
        return "reports";
    }

    @RequestMapping(value = "/report/delete/{id}", method = RequestMethod.GET)
    public String deleteReport(@PathVariable String id, Model model) {
        reportsService.deleteReport(id);
        model.addAttribute("reports", reportsService.getAll());
        return "reports";
    }

    @RequestMapping("report/{id}")
    public String showReport(@PathVariable String id, Model model) {
        model.addAttribute("report", reportsService.getReport(id));
        return "report";
    }

    @RequestMapping(value = "/report", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void report(Model model, @RequestParam("interestId") String interestId,
                       @RequestParam("content") String content,
                       @RequestParam("type") String reportType) {
        String userName = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = usersService.getUserByUsername( userName );
        Report report = new Report(interestId, content, new Date(), Report.ReportType.valueOf(reportType), user);
        reportsService.saveReport(report);
    }

}
