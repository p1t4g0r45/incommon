package ic.controllers;

import ic.domain.users.LatLng;
import ic.domain.users.User;
import ic.services.UsersService;
import ic.utility.FileUtility;
import org.aspectj.util.FileUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.validation.Valid;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.beans.PropertyEditorSupport;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by piotrek on 15.04.2016.
 */

@Controller
public class RegisterController {

    private UsersService usersService;

    @Autowired
    public void setUsersService(UsersService usersService) {
        this.usersService = usersService;
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    String registerPage(Model model) {
        model.addAttribute("user", new User());
        return "register";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    String doRegister(@ModelAttribute("user") @Valid User user, @RequestParam("avatar-image") MultipartFile file,
                      BindingResult result, Errors errors) {
        boolean correct = true;
        if(usersService.getUserByUsername(user.getUsername()) != null){
            correct = false;
            errors.reject("username.exists", "Username already exists");
        }
        if (result.hasFieldErrors("username")) {
            correct = false;
            errors.reject("username.invalid", "Username is empty");
        }
        if (result.hasFieldErrors("password")) {
            correct = false;
            errors.reject("password.invalid", "Password is empty");
        }
        try {
            byte[] imageBytes = FileUtility.multiPartFile2ByteArray(file, 128, 128);
            if(imageBytes != null){
                user.setAvatar(imageBytes);
            }
        }
        catch (IOException e){
            correct = false;
            errors.reject("image.error", "Error processing image");
        }
        if(!correct){
            return "register";
        }
        try {
            usersService.addUser(user);
        } catch (DataAccessException e){
            errors.reject("internal.error", "Internal error");
            return "register";
        }
        return "redirect:/login";
    }

    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {

        dataBinder.registerCustomEditor(Date.class, new PropertyEditorSupport() {
            @Override
            public void setAsText(String value) {
                try {
                    setValue(new SimpleDateFormat("dd/MM/yyyy").parse(value));
                } catch (ParseException e) {
                    setValue(null);
                }
            }
        });

    }

}
