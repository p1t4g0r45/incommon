package ic.controllers;

import javax.validation.Valid;
import java.util.UUID;

import ic.domain.users.User;
import ic.repositories.UsersRepository;
import ic.services.UsersService;
import ic.utility.MailUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by piotrek on 19.05.2016.
 */

@Controller
public class PasswordRecoveryController {

    private static final Logger LOGGER = LoggerFactory.getLogger( PasswordRecoveryController.class );

    private UsersService usersService;
    private PasswordEncoder passwordEncoder;
    private UsersRepository usersRepository;
    private JavaMailSender javaMailSender;

    @Autowired
    public void setUsersService( UsersService usersService ) {
        this.usersService = usersService;
    }

    @Autowired
    public void setPasswordEncoder( PasswordEncoder passwordEncoder ) {
        this.passwordEncoder = passwordEncoder;
    }

    @Autowired
    public void setUsersRepository( UsersRepository usersRepository ) {
        this.usersRepository = usersRepository;
    }

    @Autowired
    public void setJavaMailSender( JavaMailSender javaMailSender ) {
        this.javaMailSender = javaMailSender;
    }

    @RequestMapping( value = "/passwordrecovery", method = RequestMethod.GET )
    String passwordRecoveryPage( Model model ) {
        model.addAttribute( "user", new User() );
        return "passwordrecovery";
    }

    @RequestMapping( value = "/passwordrecovery", method = RequestMethod.POST )
    String remindPassword(@ModelAttribute( "user" ) @Valid User user, Errors errors ) {

        User persistenceUser = usersService.getUserByUsername( user.getUsername() );

        if ( persistenceUser != null && persistenceUser.getMail() != null &&
             persistenceUser.getMail().equals( user.getMail() ) ) {

            // generate and change password
            String newPassword = UUID.randomUUID().toString().substring( 0, 5 );
            persistenceUser.setPassword( passwordEncoder.encode( newPassword ) );
            usersRepository.save( persistenceUser );

            // generate and send mail
            javaMailSender.send( MailUtility.getPasswordRecoveryMessage( user, newPassword ) );

            LOGGER.info( "A new password was requested and sent to " + user.getUsername() + "." );
        }
        else {
            LOGGER.info( "Invalid user new password request." );
        }

        errors.reject( "post.message",
                       "An e-mail with new password will be sent if you provided correct login and mail address." );
        return "passwordrecovery";
    }

}
