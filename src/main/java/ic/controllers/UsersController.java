package ic.controllers;

import ic.domain.interests.InterestLikeLevel;
import ic.domain.users.User;
import ic.services.UsersService;
import ic.utility.InterestsUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

@Controller
public class UsersController {

    private static final Logger LOGGER = LoggerFactory.getLogger( UsersController.class );

    private UsersService usersService;

    @Autowired
    public void setUsersService(UsersService usersService) {
        this.usersService = usersService;
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public String getUsers(Model model) {
        model.addAttribute("users", usersService.getAllUsers());
        return "users";
    }

    @RequestMapping("user/{username}")
    public String showUser(@PathVariable String username, Model model, HttpServletResponse response) {
        User user = usersService.getUserByUsername(username);
        model.addAttribute("user", user);

        User activeUser = usersService.getUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName());

        if(!activeUser.getPotentialFriendsNames().contains(username) && !username.equals(activeUser.getUsername())){
            try {
                response.sendError(403);
            } catch (IOException e) {
                LOGGER.error("cannot redirect", e);
            }
            // for safety reasons
            return "index";
        }

        if (!username.equals(activeUser.getUsername())) {
            Map<InterestLikeLevel, Set<String>> commonEquallyLikedInterestsNames = new TreeMap<>(( InterestLikeLevel one, InterestLikeLevel two ) -> two.getFactor().compareTo(one.getFactor()) );
            Map<InterestLikeLevel, Set<String>> commonUserLikedInterestsNames = new HashMap<>();
            Map<InterestLikeLevel, Set<String>> individualUserLikedInterestsNames = new TreeMap<>(( InterestLikeLevel one, InterestLikeLevel two ) -> two.getFactor().compareTo(one.getFactor()) );

            InterestsUtility.overviewOfUserInterestsComparingToActiveUser(
                    user.getLikedInterestsNames(),
                    activeUser.getLikedInterestsNames(),
                    commonEquallyLikedInterestsNames,
                    commonUserLikedInterestsNames,
                    individualUserLikedInterestsNames
            );

            model.addAttribute("commonEquallyLikedInterestsNames", commonEquallyLikedInterestsNames);

            for (Map.Entry<InterestLikeLevel, Set<String>> entry : commonUserLikedInterestsNames.entrySet()) {
                if (entry.getKey() == InterestLikeLevel.LOVE) {
                    model.addAttribute("userLovesActiveUserLikes", entry.getValue());
                } else {
                    model.addAttribute("userLikesActiveUserLoves", entry.getValue());
                }
            }

            model.addAttribute("individualUserLikedInterestsNames", individualUserLikedInterestsNames);
        }

        return "user";
    }

    @RequestMapping("/you")
    public String currentUserProfile(Model model, HttpServletResponse response) {
        return showUser(SecurityContextHolder.getContext().getAuthentication().getName(), model, response);
    }
}
