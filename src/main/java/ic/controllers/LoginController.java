package ic.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by piotrek on 07.04.2016.
 */

@Controller
public class LoginController {

    @RequestMapping("/login")
    String loginPage() {
        return "login";
    }

}
