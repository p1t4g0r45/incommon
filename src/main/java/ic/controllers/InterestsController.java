package ic.controllers;

import java.util.*;

import ic.domain.interests.Interest;
import ic.domain.interests.InterestLikeLevel;
import ic.domain.users.User;
import ic.services.InterestsService;
import ic.services.InterestsServiceImpl;
import ic.services.UsersService;
import ic.utility.InterestsUtility;
import ic.utility.UsersUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class InterestsController {

    public static final int MAX_SEARCH_RESULTS = 1;

    private static final Map<String, InterestLikeLevel> NULL_USER_LIKED_INTERESTS_NAMES = new HashMap<>();

    private InterestsService interestsService;
    private UsersService usersService;

    @Autowired
    public void setInterestsService( InterestsService interestsService ) {
        this.interestsService = interestsService;
    }

    @Autowired
    public void setUsersService( UsersService usersService ) {
        this.usersService = usersService;
    }

    @RequestMapping( value = "/interests", method = RequestMethod.GET )
    public String getRootInterestPage(Model model ) {
        return getInterestPage( Interest.ROOT_INTEREST_NAME, model );
    }

    @RequestMapping( value = "/interests/{name}", method = RequestMethod.GET )
    public String getInterestPage( @PathVariable String name, Model model ) {
        model.addAttribute( "rootInterestName", Interest.ROOT_INTEREST_NAME );

        Interest interest = interestsService.getInterestByName( name );

        model.addAttribute( "interest", interest );

        Map<InterestLikeLevel, Map<String, Integer>> interestsLikeLevelCounts = InterestsUtility
                .newInterestsLikeLevelCounts();

        for ( String interestName : InterestsUtility.getInterestWithItsParentsAndChildren( interest ) ) {
            if ( interestName != Interest.ROOT_INTEREST_NAME ) {
                usersService.getInterestPopularityAmongUsers( interestName )
                            .entrySet()
                            .forEach( entry -> interestsLikeLevelCounts.get( entry.getKey() )
                                                                       .put( interestName, entry.getValue() ) );
            }
        }

        model.addAttribute( "likes", interestsLikeLevelCounts.get( InterestLikeLevel.LIKE ) );
        model.addAttribute( "loves", interestsLikeLevelCounts.get( InterestLikeLevel.LOVE ) );

        User user = usersService.getUserByUsername( SecurityContextHolder.getContext().getAuthentication().getName() );

        model.addAttribute( "isAuthenticated", user != null );
        model.addAttribute( "userInterests", user == null ? NULL_USER_LIKED_INTERESTS_NAMES : user.getLikedInterestsNames() );

        if ( user != null ) {
            model.addAttribute("isAdmin", user.isAdmin());
            model.addAttribute( "userInterestsRatio",
                                UsersUtility.getUserInterestsRatio( user, interestsService.getTotalInterestCount() ) );
        }

        return "interest";
    }

    @RequestMapping( value = "/your-interests", method = RequestMethod.GET )
    public String getActiveUserInterestsList( Model model ) {
        String activeUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        ArrayList<Map.Entry<String, InterestLikeLevel>> list = new ArrayList<>();
        usersService.getUserByUsername( activeUsername )
                    .getLikedInterestsNames()
                    .entrySet()
                    .stream()
                    .forEach( list::add );
        list.sort( Map.Entry.comparingByKey() );
        model.addAttribute( "interests", list );
        User user = usersService.getUserByUsername( activeUsername );
        model.addAttribute( "userInterestsRatio",
                            UsersUtility.getUserInterestsRatio( user, interestsService.getTotalInterestCount() ) );
        return "yourinterests";
    }

    @RequestMapping( value = "/userInterestsRatio", method = RequestMethod.GET )
    public String getUserInterestsRatio( Model model ) {
        User user = usersService.getUserByUsername( SecurityContextHolder.getContext().getAuthentication().getName() );
        model.addAttribute( "userInterestsRatio",
                            UsersUtility.getUserInterestsRatio( user, interestsService.getTotalInterestCount() ) );
        return "fragments/ratio";
    }

    @RequestMapping( value = "/graph-fragment", method = RequestMethod.GET )
    public String getInterestPageFragment( @RequestParam( "interestName" ) String interestName, Model model ) {
        return getInterestPage( interestName, model ) + " :: #graph"; // magic
    }

    @RequestMapping( value = "/search", method = RequestMethod.GET )
    @ResponseBody
    public List<String> search( @RequestParam( "query" ) String query ) {
        return interestsService.getInterestsNamesWithNameContaining( query, MAX_SEARCH_RESULTS + 1 );
    }

    @RequestMapping( value = "/search-interest/{name}", method = RequestMethod.GET )
    public String getSearchPage( @PathVariable String name, Model model ) {
        model.addAttribute("name", name);
        model.addAttribute("list", interestsService.getInterestsNamesWithNameContaining( name, null ));
        return "search-interest";
    }

    @RequestMapping( value = "/like", method = RequestMethod.POST )
    public String likeInterest( @RequestParam( "interestName" ) String interestName,
                                @RequestParam( "likeLevel" ) String likeLevel,
                                @RequestParam( "displayedInterestName" ) String displayedInterestName,
                                Model model ) {
        String userName = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = usersService.getUserByUsername( userName );

        Map<String, InterestLikeLevel> likedInterestsNames = user.getLikedInterestsNames();
        Optional<InterestLikeLevel> interestLikeLevel = Optional.ofNullable( likedInterestsNames.get( interestName ) );
        if ( interestLikeLevel.isPresent() && interestLikeLevel.get().getName().equals( likeLevel ) ) {
            likedInterestsNames.remove( interestName ); // unlike/unlove
        }
        else {
            likedInterestsNames.put( interestName, InterestLikeLevel.get( likeLevel ) ); // like/love
        }
        user.setLikedInterestsNames( likedInterestsNames );
        usersService.saveUser( user );

        return getInterestPageFragment( displayedInterestName, model );
    }

    @RequestMapping( value = "/add-interest", method = RequestMethod.POST )
    public String addInterest( @RequestParam( "interestName" ) String interestName,
                               @RequestParam( "parents[]" ) Set<String> parents,
                               @RequestParam( value = "children[]", required = false) Set<String> children,
                               Model model ) {
        if (children == null) {
            children = new HashSet<>();
        }

        Interest interest = interestsService.getInterestByName( interestName );

        if ( interest == null ) {
            interestsService.addInterest( interestName, parents, children);
        } else if (usersService.getUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).isAdmin()) {
            interestsService.modifyInterest(interest, parents, children);
        }

        return getInterestPageFragment( interestName, model );
    }

    @RequestMapping( value = "/rename-interest", method = RequestMethod.POST )
    public String renameInterest( @RequestParam( "oldInterestName" ) String oldInterestName,
                               @RequestParam( "newInterestName" ) String newInterestName, Model model ) {
        interestsService.renameInterest(oldInterestName, newInterestName);
        usersService.renameInterest(oldInterestName, newInterestName);

        return getInterestPageFragment( newInterestName, model );
    }

    @RequestMapping( value = "/remove-interest", method = RequestMethod.POST )
    public String removeInterest( @RequestParam( "interestName" ) String interestName, Model model ) {
        interestsService.removeInterest(interestName);
        usersService.removeInterest(interestName);

        return getInterestPageFragment(Interest.ROOT_INTEREST_NAME, model);
    }
}
