package ic.controllers;

import java.awt.image.BufferedImage;
import java.beans.PropertyEditorSupport;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import ic.domain.users.User;
import ic.repositories.UsersRepository;
import ic.services.UsersService;
import ic.utility.FileUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

@Controller
public class ProfileController {

    private static final Logger LOGGER = LoggerFactory.getLogger( ProfileController.class );

    private UsersService usersService;
    private PasswordEncoder passwordEncoder;
    private UsersRepository usersRepository;

    @Autowired
    public void setUsersService( UsersService usersService ) {
        this.usersService = usersService;
    }

    @Autowired
    public void setPasswordEncoder( PasswordEncoder passwordEncoder ) {
        this.passwordEncoder = passwordEncoder;
    }

    @Autowired
    public void setUsersRepository( UsersRepository usersRepository ) {
        this.usersRepository = usersRepository;
    }

    @RequestMapping( value = "/edit-profile", method = RequestMethod.GET )
    public String getProfile( Model model ) {
        String activeUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        model.addAttribute( "profile", usersService.getUserByUsername( activeUsername ) );
        return "profile";
    }

    @RequestMapping( value = "/saveProfile", method = RequestMethod.POST )
    public String saveProfile( User user, @RequestParam("avatar-image") MultipartFile file ) {

        String activeUsername = SecurityContextHolder.getContext().getAuthentication().getName();

        try {
            byte[] imageBytes = FileUtility.multiPartFile2ByteArray(file, 128, 128);
            if(imageBytes != null){
                user.setAvatar(imageBytes);
            } else {
                user.setAvatar(usersService.getUserByUsername(activeUsername).getAvatar());
            }
        }
        catch (IOException e){
            LOGGER.info( "Invalid image. Cannot modify" );
        }

        usersService.updateUser( user );
        return "redirect:/user/" + activeUsername;
    }

    @RequestMapping( value = "/editPassword", method = RequestMethod.POST )
    public String editPassword( @RequestParam( name = "newPassword" ) String newPassword ) {
        String activeUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = usersService.getUserByUsername( activeUsername );
        user.setPassword( passwordEncoder.encode( newPassword ) );
        usersRepository.save( user );
        return "redirect:/edit-profile";
    }

    @RequestMapping("/avatar/{id}")
    @ResponseBody
    public HttpEntity<byte[]> getArticleImage(@PathVariable String id, HttpServletResponse response) {

        byte[] image = usersService.getUserByUsername(id).getAvatar();

        if(image != null) {

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.IMAGE_JPEG);
            headers.setContentLength(image.length);
            return new HttpEntity<byte[]>(image, headers);

        } else {

            try {
                response.sendRedirect("../images/unknown.png");
            } catch (IOException e){
                e.printStackTrace();
            }
            return null;

        }
    }

    @InitBinder
    public void initBinder( WebDataBinder dataBinder ) {

        dataBinder.registerCustomEditor( Date.class, new PropertyEditorSupport() {
            @Override
            public void setAsText( String value ) {
                try {
                    setValue( new SimpleDateFormat( "dd/MM/yyyy" ).parse( value ) );
                }
                catch ( ParseException e ) {
                    try {
                        SimpleDateFormat sdf = new SimpleDateFormat( "EEE MMM dd HH:mm:ss zzz yyyy", Locale.US );
                        setValue( sdf.parse( value ) );
                    }
                    catch ( ParseException e1 ) {
                        setValue( null );
                    }
                }
            }
        } );

    }
}
