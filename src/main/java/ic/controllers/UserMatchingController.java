package ic.controllers;

import ic.domain.users.AgeBounds;
import ic.domain.users.Gender;
import ic.domain.users.User;
import ic.services.UserMatchingService;
import ic.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class UserMatchingController {
    private UserMatchingService userMatchingService;
    private UsersService usersService;

    @Autowired
    public void setUserMatchingService(UserMatchingService userMatchingService) {
        this.userMatchingService = userMatchingService;
    }

    @Autowired
    public void setUsersService( UsersService usersService ) {
        this.usersService = usersService;
    }

    @RequestMapping(value = "/match", method = RequestMethod.GET)
    public String matchUsers(Model model) {
        String activeUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        User activeUser = usersService.getUserByUsername(activeUsername);
        AgeBounds preferredAgeBounds = activeUser.getPreferredAgeBounds();
        Gender preferredGender = activeUser.getPreferredGender();
        Integer preferredDistance = activeUser.getPreferredDistance();
        model.addAttribute("list", userMatchingService.getTopMatchedUsers(activeUsername));
        model.addAttribute("distance", preferredDistance);
        model.addAttribute("locationProvided", activeUser.getLatLng() != null && activeUser.getLatLng().isSet());
        model.addAttribute("ageProvided", activeUser.getBirthday() != null);
        model.addAttribute("genderProvided", activeUser.getGender() != Gender.UNDEFINED);
        if(preferredAgeBounds != null) {
            model.addAttribute("age_left", preferredAgeBounds.getLowerAgeBounds());
            model.addAttribute("age_right", preferredAgeBounds.getUpperAgeBounds());
        }
        model.addAttribute("male", preferredGender == null || preferredGender == Gender.MALE);
        model.addAttribute("female", preferredGender == null || preferredGender == Gender.FEMALE);
        return "match";
    }

    @RequestMapping(value = "/edit-preferences", method = RequestMethod.POST)
    String doRegister(Model model, @RequestParam("age_left") Integer ageLeft, @RequestParam("age_right") Integer ageRight,
                      @RequestParam("distance") Integer distance, @RequestParam("male") boolean male,
                      @RequestParam("female") boolean female) {
        String activeUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        User activeUser = usersService.getUserByUsername(activeUsername);
        activeUser.setPreferredAgeBounds((ageLeft != null && ageRight != null) ? new AgeBounds(ageLeft, ageRight) : null);
        activeUser.setPreferredDistance(distance);
        activeUser.setPreferredGender(male ? female ? null : Gender.MALE : female ? Gender.FEMALE : Gender.UNDEFINED);
        usersService.saveUser(activeUser);
        return matchUsers(model);
    }

}
