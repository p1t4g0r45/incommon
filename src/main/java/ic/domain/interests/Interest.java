package ic.domain.interests;

import org.mongodb.morphia.annotations.Entity;
import org.springframework.data.annotation.Id;

import java.util.Set;

@Entity
public class Interest {
    public static final String ROOT_INTEREST_NAME = "Interest";

    @Id
    private String name;

    private Set<String> parentsNames;
    private Set<String> childrenNames;

    public Interest(String name, Set<String> parentsNames, Set<String> childrenNames) {
        this.name = name;
        this.parentsNames = parentsNames;
        this.childrenNames = childrenNames;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<String> getParentsNames() {
        return parentsNames;
    }

    public void setParentsNames(Set<String> names) {
        this.parentsNames = names;
    }

    public void addParentName(String name) {
        parentsNames.add(name);
    }

    public void removeParentName(String name) {
        parentsNames.remove(name);
    }

    public Set<String> getChildrenNames() {
        return childrenNames;
    }

    public void setChildrenNames(Set<String> names) {
        this.childrenNames = names;
    }

    public void addChildName(String name) {
        childrenNames.add(name);
    }

    public void removeChildName(String name) {
        childrenNames.remove(name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj instanceof Interest) {
            Interest other = (Interest) obj;
            return other.name.equals(name);
        }

        return false;
    }
}
