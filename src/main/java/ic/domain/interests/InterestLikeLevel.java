package ic.domain.interests;

public enum InterestLikeLevel {
    LIKE("Like", 1.0),
    LOVE("Love", 3.0);

    private String name;
    private double factor;

    InterestLikeLevel(String name, double factor) {
        this.name = name;
        this.factor = factor;
    }

    public String getName() {
        return name;
    }

    public Double getFactor() {
        return factor;
    }

    public static InterestLikeLevel get(String name) {
        for (InterestLikeLevel likeLevel : values()) {
            if (likeLevel.name.equals(name)) {
                return likeLevel;
            }
        }
        throw new IllegalArgumentException(name);
    }
}
