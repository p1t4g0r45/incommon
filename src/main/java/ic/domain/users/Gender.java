package ic.domain.users;

public enum Gender {
    UNDEFINED(""),
    MALE("Male"),
    FEMALE("Female");

    private String name;

    Gender(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
