package ic.domain.users;

public class AgeBounds {
    private final int lowerAgeBounds;
    private final int upperAgeBounds;

    public AgeBounds(int lowerAgeBounds, int upperAgeBounds) {
        this.lowerAgeBounds = lowerAgeBounds;
        this.upperAgeBounds = upperAgeBounds;
    }

    public int getLowerAgeBounds() {
        return lowerAgeBounds;
    }

    public int getUpperAgeBounds() {
        return upperAgeBounds;
    }
}
