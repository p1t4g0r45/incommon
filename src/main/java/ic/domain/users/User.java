package ic.domain.users;

import ic.domain.interests.Interest;
import ic.domain.interests.InterestLikeLevel;
import ic.services.MongoUserDetailsService;
import org.hibernate.validator.constraints.NotEmpty;
import org.mongodb.morphia.annotations.Entity;
import org.springframework.data.annotation.Id;

import javax.validation.constraints.NotNull;
import java.util.*;

/**
 * Created by ludwikbukowski on 05/04/16.
 */
@Entity
public class User {

    @NotNull
    @NotEmpty
    @Id
    private String username;

    @NotNull
    @NotEmpty
    private String password;

    private String name;
    private String surname;
    private byte[] avatar;

    private Gender gender;
    private Date birthday;
    private LatLng latLng;
    private Map<String, InterestLikeLevel> likedInterestsNames;

    private String mail;

    private Set<String> roles;

    private AgeBounds preferredAgeBounds;
    private Integer preferredDistance;
    private Gender preferredGender;

    private Set<String> potentialFriendsNames;

    // required by edit user post method
    public User() {
        this.likedInterestsNames = new HashMap<>();
        this.roles = new HashSet<>();
        this.potentialFriendsNames = new HashSet<>();
    }

    // required by edit user post method
    public void setUsername(String username) {
        this.username = username;
    }

    public void setLikedInterestsNames(Map<String, InterestLikeLevel> likedInterestsNames) {
        this.likedInterestsNames = likedInterestsNames;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public byte[] getAvatar() {
        return avatar;
    }

    public void setAvatar(byte[] avatar) {
        this.avatar = avatar;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Date getBirthday() {
        return birthday;
    }

    public Long getAge() {
        if (birthday == null) {
            return null;
        }

        Calendar dob = Calendar.getInstance();
        dob.setTime(birthday);
        Calendar today = Calendar.getInstance();
        long age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR))
            age--;
        return age;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public Map<String, InterestLikeLevel> getLikedInterestsNames() {
        return likedInterestsNames;
    }

    public InterestLikeLevel getInterestNameLikeLevel(String name) {
        return likedInterestsNames.get(name);
    }

    public void addInterest(Interest interest, InterestLikeLevel likeLevel) {
        likedInterestsNames.put(interest.getName(), likeLevel);
    }

    public void removeInterest(Interest interest) {
        likedInterestsNames.remove(interest.getName());
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public Set<String> getRoles() {
        return roles;
    }

    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }

    public AgeBounds getPreferredAgeBounds() {
        return preferredAgeBounds;
    }

    public void setPreferredAgeBounds(AgeBounds preferredAgeBounds) {
        this.preferredAgeBounds = preferredAgeBounds;
    }

    public Integer getPreferredDistance() {
        return preferredDistance;
    }

    public void setPreferredDistance(Integer preferredDistance) {
        this.preferredDistance = preferredDistance;
    }

    public Gender getPreferredGender() {
        return preferredGender;
    }

    public void setPreferredGender(Gender preferredGender) {
        this.preferredGender = preferredGender;
    }

    public boolean isAdmin() {
        return getRoles().contains(MongoUserDetailsService.ADMIN_ROLE);
    }

    public Set<String> getPotentialFriendsNames() {
        return potentialFriendsNames;
    }

    public void setPotentialFriendsNames(Set<String> potentialFriendsNames) {
        this.potentialFriendsNames = potentialFriendsNames;
    }

}
