package ic.domain.users;

/**
 * Created by baran on 22.05.16.
 */
public class LatLng {
    private Double lat;
    private Double lng;

    public LatLng() {
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public Double getLat() {
        return lat;
    }

    public Double getLng() {
        return lng;
    }

    public boolean isSet() {
        return lat != null || lng != null; // default values
    }
}
