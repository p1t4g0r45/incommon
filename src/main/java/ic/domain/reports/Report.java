package ic.domain.reports;

import ic.domain.users.User;
import org.mongodb.morphia.annotations.Entity;
import org.springframework.data.annotation.Id;

import java.util.Date;

@Entity
public class Report {

    public enum ReportType {
        NAME_MISTAKE("Name mistake"), INSULT("Insult"), WRONG_RELATIONS("Wrong relations"),
        MISSING_RELATIONS("Missing relations"), ALREADY_EXISTS("Already exists"), OTHER("Other");

        private String caption;

        ReportType(String caption) {
            this.caption = caption;
        }

        public String getCaption() {
            return caption;
        }
    }

    @Id
    public String id;
    private User reporter;
    private String content;
    private Date issueDate;
    private ReportType reportType;
    private String interestName;

    public Report(String interestName, String content, Date issueDate, ReportType reportType, User reporter) {
        this.interestName = interestName;
        this.content = content;
        this.issueDate = issueDate;
        this.reportType = reportType;
        this.reporter = reporter;
    }

    public ReportType getReportType() {
        return reportType;
    }

    public void setReportType(ReportType reportType) {
        this.reportType = reportType;
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public User getReporter() {
        return reporter;
    }

    public void setReporter(User reporter) {
        this.reporter = reporter;
    }

    public String getInterestName() {
        return interestName;
    }

    public void setInterestName(String interestName) {
        this.interestName = interestName;
    }
}
